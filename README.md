# PWM

PWM is a web based password manager developd using Express, React and MongoDB

## Installation

Use npm to install dependencies.

```bash
npm install # both in client and server folders
```

- In /server/config folder create a conf.js file like the conf.example.js
- Create a mongodb instance with your preferred method
- Copy the mongodb URI to conf.js

## Usage

```bash
npm run start-dev # for server
npm run start # for client
```

After running these commands go to localhost:3030 and start using the app.


## License
[MIT](https://choosealicense.com/licenses/mit/)
