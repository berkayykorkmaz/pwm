import {Form, Button, Container, Row, Col} from 'react-bootstrap'
import  { useState } from 'react';
import CryptoJS from 'crypto-js';
import axios from 'axios';

function ManageVault() {
  const [vaultName, setVaultName] = useState('');
  const [vaultContent, setVaultContent] = useState(null);
  const [passphrase, setPassPhrase] = useState('');

  const getVault = (e) => {
    e.preventDefault()
    const name = e.target.querySelector('#vault-name').value;
    const hashedVaultName = CryptoJS.SHA1(name).toString();
    const passphraseForm = e.target.querySelector('#vault-pass').value;
    axios.get(`${process.env.REACT_APP_API_URL}/vault`, {
      params: {
        vault: hashedVaultName
      }
    })
    .then(function (response) {
      let vault = ''
      try {
        vault = CryptoJS.AES.decrypt(response.data.content, passphraseForm).toString(CryptoJS.enc.Utf8);
        vault = JSON.parse(vault)
        setVaultName(name)
        setVaultContent(vault)
        setPassPhrase(passphraseForm)
      } catch (error) {
        alert('Wrong passphrase!')
      }
    })
    .catch(function (error) {
      alert('No vault found!');
    });
  }

  const saveUpdatedVault = (e) => {
    e.preventDefault()
    const encryptedVaultContent = CryptoJS.AES.encrypt(JSON.stringify(vaultContent), passphrase).toString();
    const hashedVaultName = CryptoJS.SHA1(vaultName).toString();
    let data = {
      name: hashedVaultName,
      content: encryptedVaultContent
    }
    axios.put(`${process.env.REACT_APP_API_URL}/vault`, data)
    .then(function (response) {
      setVaultName('')
      setVaultContent(null)
      setPassPhrase('')
      alert('Successfully updated vault')
    })
    .catch(function (error) {
      alert(error)
    });
  }

  const handleChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...vaultContent];
    list[index][name] = value;
    setVaultContent(list);
  };
  
  const handleRemoveVault = () => {
    const hashedVaultName = CryptoJS.SHA1(vaultName).toString();
    axios.delete(`${process.env.REACT_APP_API_URL}/vault`, {
      data: {
        name: hashedVaultName
      }
    })
    .then(function (response) {
      setVaultName('')
      setVaultContent(null)
      setPassPhrase('')
      alert('Successfully deleted vault: ' + vaultName)
    })
    .catch(function (error) {
      alert(error)
    });
  };

  const handleRemove = index => {
    const list = [...vaultContent];
    list.splice(index, 1);
    setVaultContent(list);
  };
  
  const handleAdd = () => {
    setVaultContent([...vaultContent, { key: '', password: '' }]);
  };

  const handleCancel = () => {
    setVaultContent(null);
    setVaultName('');
    setPassPhrase('');
  };

  return (
    <div className="manage-vault">
      <Container fluid="md" className="h-100">
        <Row className="h-100 align-items-center">
        {!vaultName && !vaultContent &&
          <Col md={{span: 6, offset: 3}}>
            <Form id="create-vault-form" onSubmit={(e) => getVault(e)}>
              <Form.Group className="mb-3" controlId="vault-name">
                <Form.Label className="font-weight-bold">Vault Name</Form.Label>
                <Form.Control type="text" minLength="4" required />
              </Form.Group>
              <Form.Group className="mb-4" controlId="vault-pass">
                <Form.Label className="font-weight-bold">Passphrase</Form.Label>
                <Form.Control type="password" required/>
              </Form.Group>
              <Button variant="primary" className="mb-3" block type="submit">
                Get Vault
              </Button>
            </Form>
          </Col>
        }
        {vaultName && vaultContent &&
          <Col md={{span: 8, offset: 2}}>
              <h3 className="mb-4"><b>Manage Vault: </b> <span className="text-primary">{vaultName}</span></h3>
              <Form onSubmit={(e) => saveUpdatedVault(e)}>
                <h5>Entries</h5>
                <Form.Group className="mb-4">
                  {vaultContent.map((x, i) => {
                    return (
                      <Form.Row className="mb-3" key={i}>
                        <Col md={{span: 4}}>
                          <Form.Control required name="key" placeholder="Key (e.g. gmail)" value={x.key} onChange={e => handleChange(e, i)}/>
                        </Col>
                        <Col md={{span: 6}}>
                          <Form.Control required name="password" placeholder="Password" value={x.password} onChange={e => handleChange(e, i)}/>
                        </Col>
                        <Col md={{span: 2}} className="d-flex">
                          {vaultContent.length !== 1 && <Button variant="danger" className="flex-fill" onClick={() => handleRemove(i)}>x</Button>}
                          {vaultContent.length - 1 === i && <Button variant="secondary" className="ml-1 flex-fill" onClick={handleAdd}>+</Button>}
                        </Col>
                      </Form.Row>
                    );
                  })}
                </Form.Group>
                <Form.Row>
                  <Col>
                    <Button variant="primary" type="submit" block>
                      Save Vault
                    </Button>
                  </Col>
                  <Col>
                    <Button variant="warning" onClick={handleRemoveVault} block>
                      Remove Vault
                    </Button>
                  </Col>
                  <Col>
                    <Button variant="danger" onClick={handleCancel} block>
                      Cancel
                    </Button>
                  </Col>
                </Form.Row>
              </Form>
            </Col>
        }
        </Row>
      </Container>
    </div>
  );
}

export default ManageVault;
