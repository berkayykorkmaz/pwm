import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { withRouter, Route, Switch } from 'react-router-dom';
import CreateVault from './CreateVault';
import ManageVault from './ManageVault';
import  Logo from './logo_no_bg.svg';
import {Image, Nav} from 'react-bootstrap'

function App(props) {
  return (
    <main className="App">
      <header className="d-flex flex-column align-items-center justify-content-center py-3">
        <Image src={Logo} style={{width: '50px'}} />
        <Nav
          activeKey="/"
        > 
          <Nav.Item>
            <Nav.Link href="/" disabled={props.location.pathname === '/'}>Create Vault</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link href="/manage" disabled={props.location.pathname === '/manage'}>Manage Vault</Nav.Link>
          </Nav.Item>
        </Nav>
      </header>
      <Switch>
        <Route path="/" component={CreateVault} exact />
        <Route path="/manage" component={ManageVault} />
      </Switch>
    </main>
  );
}

export default withRouter(App);
