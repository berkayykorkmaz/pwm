import {Form, Button, Container, Row, Col} from 'react-bootstrap'
import axios from 'axios'
import  { useState } from 'react';
import CryptoJS from 'crypto-js';

function CreateVault(props) {
  const [vaultName, setVaultName] = useState(null);
  const [vaultContent, setVaultContent] = useState([{ key: '', password: '' }]);

  const createNewVault = (e) => {
    e.preventDefault()
    const name = e.target[0].value
    const hashedVaultName = CryptoJS.SHA1(name).toString();
    axios.get(`${process.env.REACT_APP_API_URL}/allowed`, {
      params: {
        vault: hashedVaultName
      }
    })
    .then(function (response) {
      if (response.status < 400) {
        setVaultName(name)
      } else {
        throw 'error'
      }
    })
    .catch(function (error) {
      alert('Not allowed! Vault already exists.');
    });
  }

  const saveNewVault = (e) => {
    e.preventDefault()
    const passphrase = e.target.querySelector('#passphrase').value
    const encryptedVaultContent = CryptoJS.AES.encrypt(JSON.stringify(vaultContent), passphrase).toString();
    const hashedVaultName = CryptoJS.SHA1(vaultName).toString();
    let data = {
      name: hashedVaultName,
      content: encryptedVaultContent
    }
    axios.post(`${process.env.REACT_APP_API_URL}/vault`, data)
    .then(function (response) {
      alert('Successfully created vault')
    })
    .catch(function (error) {
      alert(error)
    });
    setVaultName(null)
    setVaultContent([{ key: '', password: '' }])
  }

  const handleChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...vaultContent];
    list[index][name] = value;
    setVaultContent(list);
  };
  
  const handleRemove = index => {
    const list = [...vaultContent];
    list.splice(index, 1);
    setVaultContent(list);
  };
  
  const handleAdd = () => {
    setVaultContent([...vaultContent, { key: '', password: '' }]);
  };
  

  const handleCancel = () => {
    setVaultContent([{ key: '', password: '' }]);
    setVaultName(null);
  };

  return (
    <div className="create-vault">
      <Container fluid="md" className="h-100">
        <Row className="h-100 align-items-center">
          {!vaultName && 
            <Col md={{span: 6, offset: 3}}>
              <Form id="create-vault-form" onSubmit={e => createNewVault(e)}>
                <Form.Group className="mb-4" controlId="formBasicEmail">
                  <Form.Label className="font-weight-bold">Vault Name</Form.Label>
                  <Form.Control type="text" minLength="4" required />
                  <Form.Text className="text-muted">Choose a unique and easy to memorize name for your vault.</Form.Text>
                </Form.Group>
                <Button variant="primary" className="mb-3" block type="submit">
                  Create Vault
                </Button>
              </Form>
            </Col>
          }
          {vaultName && 
            <Col md={{span: 8, offset: 2}}>
              <h3 className="mb-4"><b>Vault: </b> <span className="text-primary">{vaultName}</span></h3>
              <Form onSubmit={(e) => saveNewVault(e)}>
                <h5>Entries</h5>
                <Form.Group className="mb-4">
                  {vaultContent.map((x, i) => {
                    return (
                      <Form.Row className="mb-3" key={i}>
                        <Col md={{span: 4}}>
                          <Form.Control required name="key" placeholder="Key (e.g. gmail)" value={x.key} onChange={e => handleChange(e, i)}/>
                        </Col>
                        <Col md={{span: 6}}>
                          <Form.Control required name="password" placeholder="Password" value={x.password} onChange={e => handleChange(e, i)}/>
                        </Col>
                        <Col md={{span: 2}} className="d-flex">
                          {vaultContent.length !== 1 && <Button variant="danger" className="flex-fill" onClick={() => handleRemove(i)}>x</Button>}
                          {vaultContent.length - 1 === i && <Button variant="secondary" className="ml-1 flex-fill" onClick={handleAdd}>+</Button>}
                        </Col>
                      </Form.Row>
                    );
                  })}
                </Form.Group>
                <Form.Group className="mb-4">
                  <h5>Your Secret Passphrase</h5>
                  <Form.Control required id="passphrase" placeholder=""/>
                </Form.Group>
                <Form.Row>
                  <Col>
                    <Button variant="primary" type="submit" block>
                      Save Vault
                    </Button>
                  </Col>
                  <Col>
                    <Button variant="danger" onClick={handleCancel} block>
                      Cancel
                    </Button>
                  </Col>
                </Form.Row>
              </Form>
            </Col>
          }
        </Row>
      </Container>
    </div>
  );
}

export default CreateVault;
