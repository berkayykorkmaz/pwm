const conf = require('./config/conf')
const express = require('express')
const port = 3000
const app = express()
const mongoose = require('mongoose');
var cors = require('cors')
app.use(cors())
app.use(express.json())


const vaultSchema = new mongoose.Schema({
  name: String,
  content: String
});
const Vault = mongoose.model('Vault', vaultSchema);

mongoose
	.connect(conf.database, { useNewUrlParser: true })
	.then(() => {
    app.get('/ping', (req, res) => {
      res.send('pong')
    })
  
    app.post('/vault', async (req, res) => {
      let vault = await Vault.findOne({ name: req.body.name })
      if (vault) {
        res.sendStatus(400)
      } else {
        vault = new Vault({
          name: req.body.name,
          content: req.body.content,
        })
        await vault.save()
        res.sendStatus(201)
      }
    })

    app.put('/vault', async (req, res) => {
      let vault = await Vault.findOne({ name: req.body.name })
      if (vault) {
        vault.content = req.body.content
        await vault.save()
        res.sendStatus(201)
      } else {
        res.sendStatus(400)
      }
    })

    app.delete('/vault', async (req, res) => {
      Vault.findOne({ name:req.body.name }).remove().exec();
      res.sendStatus(201)
    })
    
    app.get('/vault', async (req, res) => {
      const vaultName = req.query.vault
      const vault = await Vault.findOne({ name: vaultName })
      if (vault) {
	      res.send(vault)
      } else {
        res.sendStatus(404)
      }
    })

    app.get('/allowed', async (req, res) => {
      const vaultName = req.query.vault
      const vault = await Vault.findOne({ name: vaultName })
      if (vault) {
        res.sendStatus(403)
      } else {
        res.sendStatus(200)
      }
    })
    
    app.listen(port, () => {
      console.log(`Server listening at http://localhost:${port}`)
    })
	})
